#!/usr/bin/lua

local precaution=
    [[As you may know, Sharkive is to be deprecated anytime soon. So here is a simple tool to build the Sharkive's cheat database.]]
local postcaution=
    [[All your base... err. All your cheats are in /3ds. Copy those into your 3ds's sd:/luma/titles/ and use Rosalina menu to activate cheats. Remember, this script is a crude tool, it works for me, but YMMV, so use it at your own risk.]]

local path = "Sharkive/3ds/"
local out = "3ds/"

function convert(filename)
    local dirName = filename:sub(0, 16)
    print(dirName)
    os.execute("mkdir "..out..dirName)
    os.execute("cp "..path..filename.." "..out..dirName.."/cheats.txt")
end

print(precaution.."\n")

os.execute("git clone https://github.com/FlagBrew/Sharkive.git")
os.execute("mkdir "..out)
for filename in io.popen("ls "..path):lines() do
    if filename:find("%.txt$") then 
        convert(filename)
    end
end
os.execute("rm -rf Sharkive")

print("\n"..postcaution)
