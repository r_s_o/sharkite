# SHARKITE


## Что 

Lua-сценарий для построения библиотеки читов для 3ds из базы Sharkive.

Если ваша консоль не прошита или вы не знаете, что такое luma и Rosalina, этот файл вам не нужен.

(NB: Если у вас нет 3ds, скорее всего, я не хочу вас знать.)


## Как

Скачать, chmod +x, запустить. Готовые читы будут лежать в /3ds, скопируйте их на карту памяти приставки в /luma/titles.

Формат базы данных Sharkive иногда меняется. Если что-то сломалось - кисмет.


## Кто

r.s.o.aliev@gmail.com

